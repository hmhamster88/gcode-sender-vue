'use strict'

import { app, BrowserWindow, ipcMain, dialog } from 'electron'
import Communicator from './Communicator'
import * as fs from 'fs'

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

const communicator = new Communicator()
let mainWindow

function openFile (fileName) {
  fs.readFile(fileName, (err, data) => {
    if (err) {
      mainWindow.webContents.send('error', err)
      return
    }
    mainWindow.webContents.send('file-opened', data.toString())
  })
}

communicator.on('connect', () => {
  mainWindow.webContents.send('connected', communicator.address)
})

communicator.on('data', (data) => {
  mainWindow.webContents.send('data-received', data)
})

communicator.on('error', (error) => {
  mainWindow.webContents.send('error', error)
})

const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 563,
    useContentSize: true,
    width: 1000
  })

  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

ipcMain.on('connect', (event, address) => {
  communicator.connect(address)
})

ipcMain.on('write', (event, data) => {
  communicator.write(data)
})

ipcMain.on('open-file', (event) => {
  dialog.showOpenDialog({properties: ['openFile']}, (filesNames) => {
    openFile(filesNames[0])
  })
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */

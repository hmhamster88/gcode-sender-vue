var net = require('net')

export default class Communicator {
  constructor () {
    this.address = ''
    this.listeners = []
  }

  connect (address) {
    this.close()
    this.address = address
    const addressParts = this.address.split(':')
    if (addressParts.length === 2) {
      this.client = new net.Socket()
      this.client.on('connect', (param) => {
        this.fireEvent('connect', param)
      })
      this.client.on('error', (param) => {
        this.fireEvent('error', param)
      })
      this.client.on('data', (param) => {
        this.fireEvent('data', param)
      })
      this.client.connect({port: Number(addressParts[1]), host: addressParts[0]})
    }
  }

  on (event, listener) {
    if (!this.listeners[event]) {
      this.listeners[event] = []
    }
    this.listeners[event].push(listener)
  }

  fireEvent (event, parameter) {
    if (!this.listeners[event]) {
      return
    }
    const eventListeners = this.listeners[event]
    for (const eventListener of eventListeners) {
      eventListener(parameter)
    }
  }

  write (text) {
    if (this.client) {
      this.client.write(text)
      console.log(text)
    }
  }

  close () {
    if (this.client) {
      this.client.end()
      this.client = null
    }
  }
}
